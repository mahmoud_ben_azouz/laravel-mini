<div class="blog-masthead">
    <div class="container">
        <nav class="nav blog-nav">
            <a class="nav-link active" href="/">Home</a>
            @if(Auth::check())
            <a class="nav-link" href="/posts">Posts</a>
                <a class="nav-link" href="/tags">Manage</a>
            <a class="nav-link" href="/posts/create">Create a post</a>
                <a class="nav-link" href="/tags/create">Create a tag</a>
            <a class="nav-link ml-auto" href="#">{{Auth::user()->name}}</a>
            <a class="nav-link" href="/logout">Logout</a>
            @endif
            @if (! Auth::check())
                <a class="nav-link ml-auto" href="/login">Login</a>
                <a class="nav-link" href="/register">Register</a>
            @endif
        </nav>
    </div>
</div>