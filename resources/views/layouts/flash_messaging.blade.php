<style>
    div#flash-message
    {
        position: fixed ;
        right: 20px;
        bottom: 20px;
        z-index: 10;
    }
</style>
<script>
    $(document).ready(function(){
        $('#flash-message').delay(1000).fadeOut(500);
    });
</script>
<div id="flash-message" class="alert alert-success" role="alert">
    {{$flash}}

</div>
<script type="text/javascript">
    //$('#flash-message').delay(500).fadeOut(500);
    //alert('Your post has now been published.')
</script>