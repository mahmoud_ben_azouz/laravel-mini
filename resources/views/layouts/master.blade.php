
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="description" content="">
   <meta name="author" content="">

   <title>Blog Template for Bootstrap</title>
   <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/blog/">

   <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->

    <!-- Custom styles for this template -->
   <link href="/css/album.css" rel="stylesheet">
</head>

<body>



@include('layouts.nav')
@if ($flash = session('message'))
    @include('layouts.flash_messaging')
@endif
<div class="blog-header">
   <div class="container">
      <h1 class="blog-title">The Bootstrap Blog</h1>
      <p class="lead blog-description">An example blog template built with Bootstrap.</p>
   </div>
</div>

<div class="container">
    <div class="row">
        @yield('content')
        @include('layouts.sidebar')
    </div>



</div><!-- /.container -->

@include('layouts.footer')

</body>

</html>
