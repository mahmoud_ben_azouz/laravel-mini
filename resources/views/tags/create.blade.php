@extends('layouts.master')

@section('content')
    <div class="col-sm-8 blog-main">
        <h1>Create a tag</h1>
        <form method="POST" action="/tag">
            @csrf
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" id="name">
            </div>
            <div class="form-group">
                <label for="cars">Choose a post:</label>
                <select name="posts_select">
                    @foreach($posts as $post)
                    <option value="{{$post->title}}">{{$post->title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add tag</button>
            </div>
            @include('layouts.errors')
        </form>
    </div><!-- /.blog-main -->
@endsection
