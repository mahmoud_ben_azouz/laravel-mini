@extends('layouts.master')

@section('content')
    <div class="col-sm-8 blog-main">
        <h1>Create a bond</h1>
        <form method="GET" action="/tags/link">
            @csrf
            <div class="form-group select-modal">
                <label for="cars">Choose a tag:</label>
                <select name="posts_select" id="posts_select">
                    @foreach($tags as $tag)
                        <option id="{{$tag->id}}" value="{{$tag->name}}">{{$tag->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button  type="submit" class="btn btn-primary">link</button>
                <button type="button" class="btn btn-info btn-lg myBtn" id="1">Open Modal</button>
            </div>

        </form>
            @include('layouts.errors')

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                        use App\Post;
                        $option_name = "tag 1";
                        $posts = Post::whereDoesntHave('tags', function ($query) use($option_name) {
                            $query->whereName($option_name);
                        })->get();
                        echo $posts;
                        ?>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <script>
            $(document).ready(function(){
                $(".myBtn").click(function(){
                    var tag_id = $(this).attr("id");
                    console.log(tag_id);
                    $("#myModal").modal();
                });
            });
        </script>


        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>Id tag</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
            </thead>
            @foreach($tags as $tag)
            <tbody>
            <tr>
                <td>{{$tag->id}}</td>
                <td>{{$tag->name}}</td>
                <td><button type="button" class="btn btn-info btn-lg myBtn" id="{{$tag->name}}">Open Modal</button></td>
            </tr>
            </tbody>
            @endforeach
        </table>


    </div><!-- /.blog-main -->
@endsection
