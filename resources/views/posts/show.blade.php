@extends ('layouts.master')

@section('content')
    <div class="col-sm-8 blog-main">
        <h1>{{$post->title}} </h1>
        @if (count ($post->tags))
            @foreach($post->tags as $tag)
                <li><a href="/posts/tags/{{$tag->name}}">{{$tag->name}}</a></li>
            @endforeach
        @endif

        <h1>{{$post->body}} </h1><hr>

        <div class="comments">
            <ul class="list-group">
              @foreach($post->comments as $comment)
               <li class="list-group-item">
                   <h6>
                       {{$comment->created_at->diffForHumans() }} :
                   </h6>
                   <h5>{{$comment->user_comment}}</h5>
                {{$comment->body}}
               </li>
             @endforeach
            </ul>
        </div>
        <a href="/posts/">Return to Posts</a><hr>

        {{-- Add comments --}}

        <form method="POST" action="/posts/{{$post->id}}/comments">
            @csrf
            <div class="form-group">
                <textarea class="form-control" name="body" name="body"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add comments</button>
            </div>
            @include('layouts.errors')
        </form>
    </div>
@endsection
