<div class="blog-post">
    <h2 class="blog-post-title">
        <a href="/posts/{{$post->id}}">{{ $post->title }}</a>
    </h2>
    <p class="blog-post-meta">
        {{$post->user->name }} on
        {{$post->created_at->toformatteddatestring()}}</p>

    {{ $post->body }}
    <form method="POST" action="/posts/{{$post->id}}">
        @csrf
        <div class="form-group"><br>
            <!-- <button type="submit" class="btn btn-success">Edit</button> -->
            <button type="submit" class="btn btn-danger">Delete</button>
        </div>
    </form>
</div><!-- /.blog-post -->