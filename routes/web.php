<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'postController@index')->name('home');
Route::get('/about', 'aboutController@index');
Route::get('/tasks', 'tasksController@index');
Route::get('/tasks/{task}', 'tasksController@show');

Route::get('/posts', 'postController@index');
Route::get('/posts/create', 'postController@create');
Route::post('/posts', 'postController@store');
Route::get('/posts/{post}', 'postController@show');
Route::post('/posts/{post}', 'postController@delete');

Route::post('/posts/{post}/comments', 'commentController@store');


Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');

Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');

Route::get('/posts/tags/{tag}', 'TagsController@index');

Route::get('/tags/create', 'TagsController@create');
Route::post('/tag', 'TagsController@store');


Route::get('/tags', 'TagsController@manage');
Route::get('/tags/link', 'TagsController@link');
/*Route::get('/request', function(Request $request){


});*/

Route::get('/csv', 'postController@exportToExcel');
Route::get('/excel', 'postController@exportIntoCSV');







