<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
    //for mass assignment exception
    //protected $fillable = ['title', 'body']; // who has permission
    protected $guarded  = []; // who has no permission

}