<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use http\Env\Request;

class Post extends Model
{
    public function comments()
    {
        return $this->hasMany(Comment::class); // same as App\Comment
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addComment($body)
    {
        //Comment::create(['body' => $body, 'post_id' => $this->id]);
        $user_id = auth()->user()->id;

        $user_comment= auth()->user()->name;
        $this->Comments()->create(compact('body','user_id', 'user_comment'));
    }

    public function scopeFilter_model($query, $filters)
    {
        /*if ($month =$filters['month'])
        {
            $query->whereMonth('created_at', Carbon::parse($month)->month);
        }
        if ($year =$filters['year'])
        {
            $query->whereYear('created_at', Carbon::parse($year)->year);
        }*/
        if (isset($filters['month']))
        {
            $query->whereMonth('created_at', Carbon::parse($filters['month'])->month);
        }

        if (isset($filters['year']))
        {

            $query->whereYear('created_at', $filters['year']);
        }

    }

    public static function archives()
    {
        return static::selectRaw('year(created_at) year, monthname(created_at) month, year(created_at) year, count(*) published')
            ->groupBy('year', 'month')
            ->orderByRaw('min(created_at) desc')
            ->get()
            ->toArray();
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
    public static function getPost()
    {
        $records= Post::all();
        return $records;
    }






}
