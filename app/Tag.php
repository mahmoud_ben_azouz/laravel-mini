<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function postss() {
        return $this->belongsToMany(Post::class);
    }

    /**
     * @param Tag $tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }
}

