<?php

namespace App\Exports;

use App\Post;
use Maatwebsite\Excel\Concerns\FromCollection;


class PostExport implements FromCollection
{
    public function headings()
    {
        return ["Id","user_id","title","body"];

    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect(Post::getPost());
        //return Post::all();
    }
}
