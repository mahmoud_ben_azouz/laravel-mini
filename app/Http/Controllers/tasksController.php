<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class tasksController extends Controller
{
    public function index()
    {
        $tasks= [
            'task1',
            'task2',
            'task3',
            'task4'
        ];

        $tasks2= [
            'task5',
            'task6',
            'task7',
            'task8'
        ];

        //$tasks3= DB::table('tasks')->get();
        $tasks3= Task::get();

        $tasks4= Task::all();

        $tasks5= Task::find(1);

        $tasks6= $tasks5::inComplete();

        return view('tasks', compact('tasks', 'tasks2', 'tasks3', 'tasks4', 'tasks5','tasks6'));

    }

    public function show(Task $task) // find(wildcard) //Route Model Binding
    {
        //$task= Task::find($id);

        return view('show', compact('task'));

    }
}
