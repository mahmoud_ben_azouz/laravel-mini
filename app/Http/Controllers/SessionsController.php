<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;


class SessionsController extends Controller
{
    public function index()
    {

    }
    public function __construct()
    {
        $this->middleware('guest')->except(['destroy']);
    }

    public function create()
    {
        return view('sessions.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'email' => 'required|email',
            'password' => 'required',

        ]);

        $test=User::where('email', '=', $request['email'])->exists();
        if ($test)
        {
            $user_id_table = User::where('email', '=', $request['email'])->value('id');
            $user_password_table = User::where('email', '=', $request['email'])->value('password');
            if (!( $request['password'] == $user_password_table))
            {
                return back()->withErrors([
                    'message' => 'Please check your credentials and try again.'
                ]);
            }
            else
            {
                $user_login= User::find($user_id_table);
                auth()->login($user_login);
                return redirect()->home();
            }
        }
        else
        {
            return back()->withErrors([
                'message' => 'Email not found'
            ]);
        }

    }
    public function destroy()
    {
        auth()->logout();
        return redirect()->home();
    }
}
