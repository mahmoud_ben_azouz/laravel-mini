<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Post;

class TagsController extends Controller
{
    public function index(Tag $tag)
    {
        $posts = $tag->posts;
        return view('posts.index', compact('posts'));
    }
    public function create()
    {
        $posts=Post::all();
        return view('tags.create',compact('posts'));
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required|unique:tags|max:10',
        ]);

        $tag = new Tag;
        $tag->name = request('name');
        $tag->save();


        $drop_down = $request->input('posts_select');
        $post_id=Post::where('title','=',$drop_down)->pluck('id');

        $tag->posts()->attach($post_id);
        $posts=Post::all();
        return view('tags.create',compact('posts'));
    }

    public function manage(Request $request)
    {
        $posts=Post::all();
        $tags=Tag::all();
        return view('tags.manage',compact('posts','tags'));
    }

    public function link(Request $request)
    {
        $option_name = $request->posts_select;
        return $option_name;
        $posts = Post::whereDoesntHave('tags', function ($query) use($option_name) {
            $query->whereName($option_name);
        })->get();


        return $posts;
        $tags=Tag::all();
        return view('tags.manage',compact('posts','tags'));
    }





}
