<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationForm;
use App\Mail\Welcome;
use Illuminate\Http\Request;
use App\User;
use PharIo\Manifest\Email;


class RegistrationController extends Controller
{
    public function create()
    {
        return view('registration.create');
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(RegistrationForm $form)
    {
       $form->persist();
       session()->flash('message', 'Thanks for signing up!');

        return redirect()->home();

    }
}
