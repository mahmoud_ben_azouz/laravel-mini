<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use App\Post;
use Carbon\Carbon;
use App\Exports\PostExport;
use Excel;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function index()
    {
        //return session('message');
        $posts= Post::latest()
            ->filter_model(request(['month','year']))
            ->get();

        /*$posts= Post::latest();// order by the new one or switch with oldtest
        $posts= Post::orderBy('created_at', 'desc')->get();// order by the new one*/

        return view('posts.index', compact('posts'));//send all posts to view
    }

    public function create()
    {
        return view('posts.create');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store()
    {
        //dd(request()->all()); //get all inputs form

        /*create a post

        $post = new Post;
        $post->title= request('title');
        $post->body= request('body');*/

        /*save it into DB
        $post->save();*/

        /* create and save post
        Post::create([
            'title'=> request('title'),
            'body'=> request('body')
        ]);*/

        $this->validate(request(),[
            'title' => 'required|unique:posts|max:20',
            'body' => 'required|max:255'
        ]);
        /* create and save post */

        auth()->user()->publish(new Post (request(['title','body'])));
        session()->flash('message','Your post has now been published.');


        //and then redirect to the home page
        return redirect('/');
    }

    public function show(Post $post)
    {
        //$user_id= $post->user_id;
        //Comment::where('email', '=', $user_id)->first();
        //$user= User::find($user_id);
        return view('posts.show', compact('post'));
    }

    public function delete(Post $post)
    {
        $post_id= $post->id;
        $post->tags()->wherePivot('post_id','=',$post_id)->detach();

        Comment::where("post_id", "=", $post_id)->delete();

        $post->delete();
        return redirect('/');
    }

    public function exportToExcel()
    {
        return Excel::download(new PostExport, 'posts.xlsx');
    }

    public function exportIntoCSV()
    {
        return Excel::download(new PostExport, 'posts.csv');
    }



}
