<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
use App\Mail\Welcome;
use PharIo\Manifest\Email;

class RegistrationForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:users|max:20',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ];
    }
    public function persist()
    {
        // create and save user
        $user= User::create(request(['name','email','password']));
        //login user
        auth()->login($user);
        \Mail::to($user)->send(new Welcome($user));
    }
}
